package com.mastering.spring.springmvc.controller.interceptor;

import com.mastering.spring.springmvc.controller.UserController;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HandlerTimeLoggingInterceptor extends HandlerInterceptorAdapter{

    private Log logger = LogFactory.
            getLog(HandlerTimeLoggingInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
               HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute("startTime", System.currentTimeMillis());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        request.setAttribute("endTime", System.currentTimeMillis());
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        long startTime = (Long) request.getAttribute("startTime");
        long endTime = (Long) request.getAttribute("endTime");
        logger.info("Time spent in Handler in ms : "+ (endTime-startTime));
    }
}
